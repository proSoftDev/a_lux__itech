@extends('layouts.main')
@section('content')

    <style>
        @foreach($advantages as $advantage)
        .advantages .advantages-inner .advantages-content:hover .advantage-image-{{ $loop->iteration }}::before {
            content: '';
            background-image: url({{ Voyager::image($advantage->image) }});
            width: 100%;
            height: 100%;
            display: block;
            background-repeat: no-repeat;
            background-position-y: bottom;
            background-position-x: center; }

        .advantages .advantages-inner .advantages-content .advantage-image-{{ $loop->iteration }}::before {
            content: '';
            background-image: url({{ Voyager::image($advantage->icon) }});
            width: 100%;
            height: 100%;
            display: block;
            background-repeat: no-repeat;
            background-position-y: bottom;
            background-position-x: center; }
        @endforeach
    </style>

    <div class="home-banner">
        <div class="banner-sliders">
            @foreach($banners as $banner)
            <div class="banner-slide" style="background-image: url({{ Voyager::image($banner->image) }});">
                <div class="banner-content">
                    <div class="banner-logo" >
                        <img src="{{ asset('images/logo-main.png') }}" alt="">
                    </div>
                    <div class="banner-text">
                        <p>{{ $banner->content }}</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="about-company">
        <div class="container">
            <div class="about-content">
                <h1 class="home-title">{{ $page->getPage()->getFirstTitle() }}</h1>
                {!! $about->content !!}
                <a href="{{ route('about') }}" class='more-details'>@lang('texts.Подробнее')</a>
            </div>
        </div>
    </div>

    <div class="advantages">
        <div class="container">
            <h1 class="home-title">{{ $page->getPage()->getSecondTitle() }}</h1>
            <div class="advantages-inner row">
                @foreach($advantages as $advantage)
                <div class="col-xl-3 p-0 col-md-6">
                    <div class="advantages-content" >
                        <div class="advantage-image advantage-image-{{ $loop->iteration }}">
                            <img src="" alt="">
                        </div>
                        <div class="advantage-text">
                            <p>{{ $advantage->name }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="solutions">
        <div class="container">
            <h1 class="home-title">
                {{ $page->getPage()->getThirdTitle() }}
            </h1>
            <div class="solution-info">
                <div class="solutions-slider">
                    @foreach($solutions as $v)
                    <div class="solution-slide">
                        <div class="solution-image">
                            <a href="{{ Voyager::image($v->image) }}" class="solution-image">
                                <img src="{{ Voyager::image($v->image) }}" alt="">
                                <span><img src="{{ asset('images/magnifier.png') }}" alt=""></span>
                            </a>
                        </div>
                        <div class="solution-content">
                            <h1>{{ $v->name }}</h1>
                            {!! $v->short_description !!}
                            <div class="solution-btn">
                                <a href="{{ route('solution', ['id' => $v->id]) }}" class='more-details'>@lang('texts.Подробнее')</a>
                            </div>
                        </div>

                    </div>
                    @endforeach
                </div>
                <div class="solutions-link">
                    <a href="{{ route('solutions') }}">@lang('texts.Смотреть все')</a>
                </div>
            </div>
        </div>
    </div>

    <h1 class="home-title">
        {{ $page->getPage()->getForthTitle() }}
    </h1>
    <div class="certificates">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-12 col-md-6">
                    <div class="certificates-text certificates-left">
                        <h1>{{ $license->left_title }}</h1>
                        {!! $license->left_content !!}
                    </div>
                </div>
                <div class="col-xl-6 col-12 col-md-6">
                    <div class="certificates-text">
                        <h1>{{ $license->right_title }}</h1>
                        {!! $license->right_content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="certificate-bottom">
        <div class="container">
            <div class="certificates-slider ">
                @foreach($certificates as $certificate)
                <div class="certificate-slide">
                    <a href="{{ Voyager::image($certificate->image) }}" class="certificate-image">
                        <img src="{{ Voyager::image($certificate->image) }}" alt="">
                        <span><img src="{{ asset('images/magnifier2.png') }}" alt=""></span>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="advantages-bottom">
        <div class="container">
            <h1 class="home-title">
                {!! $page->getPage()->getFifthTitle() !!}
            </h1>
            <div class="row">
                @foreach($word_advantages as $v)
                <div class="col-xl-6 col-md-6">
                    <div class="advantage-list">
                        <div class="advantage-list-image">
                            <img src="{{ Voyager::image($v->icon) }}" alt="">
                        </div>
                        <div class="advantage-list-text">
                            <h1>{{ $v->name }} </h1>
                            {!! $v->content !!}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>



    <div class="contacts">
        <div class="container">
            <div class="contacts-info">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="contacts-left">
                            <h1> {!! $page->getPage()->getSixthTitle() !!}</h1>
                            <div class="contacts-content">
                                <img src="{{ asset('images/map.png') }}" alt="">
                                <p>{{ $address->content }}</p>
                            </div>
                            <div class="contacts-content">
                                <img src="{{ asset('images/phone.png') }}" alt="">
                                @foreach($telephones as $v)
                                    <a href="tel:{{ $v->text }}">{{ $v->text }}  @if(!$loop->last)/ @endif</a>
                                @endforeach
                            </div>
                            <div class="contacts-content contacts-email">
                                <img src="{{ asset('images/email.png') }}" alt="">
                                <a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <form class="contacts-right">
                            <textarea type="text" name="message" placeholder="@lang('texts.Ваше сообщение')"></textarea>
                            <input type="text" name="phone" placeholder="@lang('texts.Номер телефона')" class="contact-input">
                            <input type="text" name="name" placeholder="@lang('texts.Ваше имя')*" class="contact-input">
                            <input type="text" name="email" placeholder="@lang('texts.Ваш e-mail')*" class="contact-input">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox" name="agreement" >
                                @lang('texts.Даю согласие на обработку своих данных')
                            </label>
                            <button id="mainFeedbackBtn" type="button" class="more-details">@lang('texts.Написать')</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="partners">
        <div class="container">
            <h1 class="home-title">
                {!! $page->getPage()->getSeventhTitle() !!}
            </h1>
            @include('/partials/partners', compact('partners'))
        </div>
    </div>

@endsection
