@extends('layouts.app')
@section('content')
    <div class="banner" style='background-image:url({{ Voyager::image($solution->banner) }});'>
        <div class="container">
            <h1>{{ $solution->name }}</h1>
        </div>
    </div>

    @include('/partials/breadcrumbs')

    @foreach($blocks as $v)

        @if($v->status == 1)
            @if($loop->first)
                <div class="page-title">
                    <div class="container">
                        <h1>{{ $v->title }} </h1>
                    </div>
                </div>
                <div class="products-content">
                    <div class="container">
                        {!! $v->content !!}
                    </div>
                </div>
            @else
                <div class="smart-home">
                    <div class="container">
                        <div class="smart-home-content">
                            <h1>{{ $v->title }} </h1>
                            {!! $v->content !!}
                        </div>
                    </div>
                </div>
            @endif
        @elseif($v->status == 2)

            <div class="smart-home">
                <div class="container">
                    <div class="smart-home-content">
                        <h1>{{ $v->title }}</h1>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="smart-home-left">
                                    @if($v->relations != null)
                                        @foreach($v->relations as $relation)
                                            @if($relation->imageList)
                                                @php
                                                    $relation->imageList = $relation->imageList->translate(app()->getLocale());
                                                @endphp
                                                <div class="smart-wrapper">
                                                    <div class="smart-icons">
                                                        <img src="{{ Voyager::image($relation->imageList->image) }}" alt="">
                                                    </div>
                                                    <div class="smart-text">
                                                        <p>{{ $relation->imageList->text }}</p>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="smart-home-right">
                                    <img src="{{ Voyager::image($v->picture_list_image) }}" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        @elseif($v->status == 3)
            <div class="call-center">
                <div class="container">
                    <div class="call-content">
                        <h2>{{ $v->title }}</h2>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="call-left">
                                    {!! $v->check_list_content !!}
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="call-right">
                                    <img src="{{ Voyager::image($v->check_list_image) }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="call-center">
                <div class="container">
                    <div class="call-content">
                        <h2>{{ $v->title }}</h2>
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="call-content-bottom">
                                    {!! $v->dot_list_left !!}
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="call-content-bottom">
                                    {!! $v->dot_list_right !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    @endforeach

@endsection
