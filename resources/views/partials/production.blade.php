<div class="{{ $class }}">
    <div class="col-xl-6">
        <div class="about-slider-for about-image-gallery">
            @foreach (json_decode($production->images) as $image)
                <a href="{{ Voyager::image($image) }}" class='about-slide-image'>
                    <div class="about-slide-top">
                        <img src="{{ Voyager::image($image) }}" alt="">
                    </div>
                </a>
            @endforeach
        </div>
        <div class="about-slider-nav">

            @foreach (json_decode($production->images) as $image)
                <div class="about-slide-bottom">
                    <img src="{{ Voyager::image($image) }}" alt="" width="101" height="55">
                </div>
            @endforeach
        </div>
    </div>
    <div class="col-xl-6">
        <div class="about-content">
            <h1>{{ $page->getPage()->getFirstTitle() }}</h1>
            {!! $production->content !!}
        </div>
    </div>
</div>
