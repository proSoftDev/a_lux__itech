<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @if(isset($g_solution) && Request::segment(1) == 'solution')
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('texts.Главная')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('solutions') }}">@lang('texts.Решения')</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> {{ $g_solution->translate(app()->getLocale())->name }}</li>
                @elseif(isset($g_product) && Request::segment(1) == 'product')
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('texts.Главная')</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('products') }}">@lang('texts.Продукция')</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> {{ $g_product->translate(app()->getLocale())->title }}</li>
                @else
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('texts.Главная')</a></li>
                    <li class="breadcrumb-item active" aria-current="page"> {{ $page->getPage()->getFirstTitle() }}</li>
                @endif
            </ol>
        </nav>
    </div>
</div>
