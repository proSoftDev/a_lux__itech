@extends('layouts.app')
@section('content')

    @include('/partials/breadcrumbs')

    <div class="vacancies-page">
        <div class="container">

            <h1 class="title-info">{{ $page->getPage()->getFirstTitle() }}</h1>

            @foreach($vacancies as $vacancy)
                <div class="vacancy-content">
                    <div class="row">
                        <div class="col-xl-5 col-12 col-md-5">
                            <div class="vacancy-image">
                                <img src="{{ Voyager::image($vacancy->image) }}" alt="">
                            </div>
                        </div>
                        <div class="col-xl-7 col-12 col-md-7">
                            <div class="vacancy-info">
                                <h1>{{ $vacancy->name }}</h1>
                                <span>{{ \Carbon\Carbon::parse($vacancy->date)->format('d.m.Y') }}</span>
                                <p>@lang('texts.Заработная плата'): <b> {{ $vacancy->wage }}</b></p>
                                <p>@lang('texts.Требуемый опыт'): <b>{{ $vacancy->experience_required }}</b></p>
                                <div class="duties-content">
                                    <div class="duties-left">
                                        <p>@lang('texts.Обязанности'):</p>
                                    </div>
                                    <div class="duties-right">
                                        {!! $vacancy->responsibilities !!}
                                    </div>
                                </div>
                                {!! $vacancy->text_bottom !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>

@endsection
