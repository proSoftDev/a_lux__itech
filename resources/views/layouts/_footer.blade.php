<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-12 col-md-6">
                <div class="footer-link">

                    @if(Request::segment(1))
                        <a href="{{ route('home') }}">
                            <img src="{{ Voyager::image(setting('site.home_logotype')) }}" alt="">
                        </a>
                    @else
                        <img src="{{ Voyager::image(setting('site.home_logotype')) }}" alt="">
                    @endif

                    {!! $copyright->content !!}
                    <div class="social-links">
                        @if(setting('site.youtube') != '')
                        <a href="{{ setting('site.youtube') }}"><img src="images/youtube.png" alt=""></a>
                        @endif
                        @if(setting('site.instagram') != '')
                            <a href="{{ setting('site.instagram') }}"><img src="images/instagram.png" alt=""></a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-12 col-md-6">
                <div class="footer-link footer-link2">
                    @php $m = 0; @endphp
                    @foreach (menu('site_footer', '_json') as $menuItem)
                        @php $menuItem = $menuItem->translate(app()->getLocale()); @endphp
                        @if(strlen($menuItem->title) > 20 && $m % 2 == 0)
                            <div class="footer-sub">
                                <a href="{{ $menuItem->url }}">{{ $menuItem->title }}</a>
                            </div>
                        @else
                            @php $m++;  @endphp
                            @if($m % 2 == 1)
                                <div class="footer-sub">
                            @endif
                                <a href="{{ $menuItem->url }}">{{ $menuItem->title }}</a>
                            @if($m % 2 == 0)
                                </div>
                            @endif
                        @endif
                    @endforeach

                    @if($m % 2 != 0)
                    </div>
                    @endif
                </div>
            </div>

            <div class="col-xl-3 col-12 col-md-6">
                <div class="footer-link">
                    <div class="footer-sub">
                        <img src="images/footer-map.png" alt="">
                        <p>{{ $address->content }}</p>
                    </div>
                    <div class="footer-sub">
                        <img src="images/footer-phone.png" alt="">
                        <div class="phone-link">
                        @foreach($telephones as $v)
                            <a href="tel:{{ $v->text }}">{{ $v->text }}</a>
                        @endforeach
                        </div>
                    </div>
                    <div class="footer-sub footer-email">
                        <img src="images/footer-email.png" alt="">
                        <a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a>
                    </div>
                    <div class="footer-sub">
                        <a href="https://a-lux.kz">Разработано в <img src="{{ asset('images/a-lux.png') }}" alt="A-lux"></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-12 col-md-6">
                @if(app()->isLocale('kz'))
                    @php $lang = 'kz_KZ' @endphp
                @else
                    @php $lang = 'ru_RU' @endphp
                @endif
                <iframe class="ggmap_contact" src="{{ setting('site.map_url') }}?lang={{ $lang }}" frameborder="0" style="border:0;width: 306px; height: 204px" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?&lang=ru_RU" type="text/javascript"></script>
<script src="{{ asset('/js/main.js') }}"></script>
<script src="{{ asset('js/ajax.js') }}"></script>

</body>
</html>



