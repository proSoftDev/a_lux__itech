<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ $page->getMeta()->description }}">
    <meta name="keyword" content="{{ $page->getMeta()->keyword }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base" content="{{ url('/') }}">
    <title>{{ $page->getMeta()->title }}</title>

    <link rel="stylesheet" href="{{ asset('/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/magnific-popup.css') }}">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
</head>

<body>

    @include('/partials/header', ['class' => 'header-home', 'logo' => Voyager::image(setting('site.home_logotype'))])
