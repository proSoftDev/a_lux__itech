@extends('layouts.app')
@section('content')

    @include('/partials/breadcrumbs')

    <div class="about">
        <div class="container">

            @include('/partials/production', ['class' => 'row', 'production' => $production])
        </div>

        <div class="key-areas">
            <div class="container">
                <h1 class="home-title"> {!! $page->getPage()->getSecondTitle() !!}</h1>
                <div class="row">
                    @php $m = 0 @endphp
                    @foreach($key_areas as $v)
                        @php $m++ @endphp
                        @if($m % 2 == 1)
                            <div class="col-xl-6">
                                <div class="key-areas-text">
                                    {!! $v->content !!}
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="key-areas-image">
                                    <img src="{{ Voyager::image($v->image) }}" alt="">
                                </div>
                            </div>
                        @else
                            <div class="col-xl-6">
                                <div class="key-areas-image">
                                    <img src="{{ Voyager::image($v->image) }}" alt="">
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="key-areas-text key-areas-bottom">
                                    {!! $v->content !!}
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

    </div>

    <div class="partners partners-about">
        <div class="container">
            <h1 class="home-title">
                {{ $page->getPage()->getThirdTitle() }}
            </h1>
            @include('/partials/partners', compact('partners'))
        </div>
    </div>

@endsection
