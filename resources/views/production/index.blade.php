@extends('layouts.app')
@section('content')

    @include('/partials/breadcrumbs')

    <div class="about production">
        <div class="container">
            @include('/partials/production', ['class' => 'row', 'production' => $production])

            <div class="about-system-works">
                <h1>{{ $page->getPage()->getSecondTitle() }}</h1>
                <div class="system-slider">
                    @foreach($sliders as $slider)
                        <div class="container p-0">
                            <div class="system-slide">
                                <iframe   src="{{ $slider->url }}" frameborder="0"
                                          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                          allowfullscreen>
                                </iframe>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
