<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});



// PAGES
Route::get('/', 'HomeController@index')->name('home');
Route::get('/lang/{locale}', 'LangController@index')->name('lang');
Route::get('/about', 'AboutController@index')->name('about');

Route::get('/products', 'ProductController@index')->name('products');
Route::get('/product/{id}', 'ProductController@view')->name('product');

Route::get('/solutions', 'SolutionController@index')->name('solutions');
Route::get('/solution/{id}', 'SolutionController@view')->name('solution');

Route::get('/partners', 'PartnerController@index')->name('partners');
Route::get('/contacts', 'ContactController@index')->name('contacts');
Route::get('/letters-of-recommendation', 'LetterController@index')->name('letters');
Route::get('/vacancies', 'VacancyController@index')->name('vacancies');
Route::get('/own-productions', 'OwnProductionController@index')->name('productions');

Route::post('/request/main', 'RequestController@main')->name('main-request');
Route::post('/request/contact', 'RequestController@contact')->name('contact-request');
