<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MainRequest extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'message'];
}
