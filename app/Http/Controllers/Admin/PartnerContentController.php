<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11.06.2020
 * Time: 17:19
 */

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class PartnerContentController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }
}
