<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.06.2020
 * Time: 12:12
 */

namespace App\Http\Controllers;


use App\AboutSlider;
use App\Helpers\TranslatesCollection;
use App\OwnProduction;

class OwnProductionController extends Controller
{
    public function index(){

        $production = OwnProduction::getContent();
        $sliders = AboutSlider::getAll();
        TranslatesCollection::translate($production, app()->getLocale());

        return view('production.index', compact('production', 'sliders'));
    }
}
