<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Request as RequestForContact;
use App\MainRequest as RequestForMain;
use App\Mail\MainRequest;
use App\Mail\ContactRequest;

class RequestController extends Controller
{
    public function main(Request $request) {

        $validator = Validator::make($request->all(), [
            'phone'     => 'required|regex:/(\+77)[0-9]{9}/',
            'name'          => 'required',
            'email'         => 'required|email',
            'agreement'     => 'accepted'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForMain::create($request->all());
            Mail::to(setting('site.email'))->send(new MainRequest($model));
            return response([
                'status'    => 1,
                'title'     => trans('texts.Заявка успешно отправлен'),
                'content'   => trans('texts.В ближайшее время мы свяжемся с вами'),
            ], 200);
        }
    }


    public function contact(Request $request) {

        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'required|email'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForContact::create($request->all());
            Mail::to(setting('site.email'))->send(new ContactRequest($model));
            return response([
                'status'    => 1,
                'title'     => trans('texts.Заявка успешно отправлен'),
                'content'   => trans('texts.В ближайшее время мы свяжемся с вами'),
            ], 200);
        }
    }



}
