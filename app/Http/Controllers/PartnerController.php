<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.06.2020
 * Time: 12:01
 */

namespace App\Http\Controllers;


use App\Helpers\TranslatesCollection;
use App\Partner;
use App\PartnerContent;

class PartnerController extends Controller
{

    public function index(){

        $content = PartnerContent::getContent();
        $partners = Partner::getAll();

        TranslatesCollection::translate($content, app()->getLocale());

        return view('partners.index', compact('content', 'partners'));
    }
}
