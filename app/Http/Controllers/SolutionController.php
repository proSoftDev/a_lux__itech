<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.06.2020
 * Time: 11:55
 */

namespace App\Http\Controllers;

use App\Helpers\TranslatesCollection;
use App\Solution;
use App\SolutionContent;

class SolutionController extends Controller
{
    public function index(){

        $solutions = Solution::getAll();
        TranslatesCollection::translate($solutions, app()->getLocale());

        return view('solutions.index', compact('solutions'));
    }

    public function view($id){

        $solution = Solution::find($id);
        if($solution && $solution->status){

            $blocks = SolutionContent::getAllByProduct($id);
            TranslatesCollection::translate($solution, app()->getLocale());
            TranslatesCollection::translate($blocks, app()->getLocale());

            return view('solutions.view', compact('solution', 'blocks'));
        }else{
            abort(404);
        }
    }
}
