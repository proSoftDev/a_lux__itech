<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 20.03.2020
 * Time: 13:39
 */

namespace App\Http\Controllers;


use App\Advantage;
use App\Banner;
use App\Certification;
use App\Helpers\TranslatesCollection;
use App\KeyArea;
use App\MainAbout;
use App\MainCertificatesLicense;
use App\Partner;
use App\Solution;
use App\WorkAdvantage;

class HomeController extends Controller
{
    public function index(){

        $banners = Banner::getAll();
        $about = MainAbout::getContent();
        $advantages = Advantage::getAll();
        $solutions = Solution::getAll();
        $license = MainCertificatesLicense::getContent();
        $certificates = Certification::getAll();
        $word_advantages = WorkAdvantage::getAll();
        $partners = Partner::getAll();

        TranslatesCollection::translate($banners, app()->getLocale());
        TranslatesCollection::translate($about, app()->getLocale());
        TranslatesCollection::translate($advantages, app()->getLocale());
        TranslatesCollection::translate($license, app()->getLocale());
        TranslatesCollection::translate($word_advantages, app()->getLocale());
        TranslatesCollection::translate($solutions, app()->getLocale());

        return view('home.index', compact('banners', 'about', 'advantages', 'license',
            'certificates', 'word_advantages', 'key_areas', 'partners', 'solutions'));
    }

}
