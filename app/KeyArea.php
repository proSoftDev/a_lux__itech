<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class KeyArea extends Model
{
    use Translatable;
    protected $translatable = ['content'];

    public static function getAll(){
        return self::get();
    }
}
