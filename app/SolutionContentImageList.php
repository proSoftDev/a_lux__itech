<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SolutionContentImageList extends Model
{
    public function imageList(){
        return $this->hasOne('App\SolutionImageList','id', 'solution_image_list_id');
    }
}
