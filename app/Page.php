<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Page extends Model
{
    use Translatable;
    protected $translatable = ['meta_title','meta_description','meta_keyword'];

    public static function getByUrl($url){
        return self::where('url', $url)->first();
    }

    public function getTitle(){
        return $this->hasMany('App\Title');
    }

    public function getFirstTitle(){
        return $this->getTitle() ? $this->getTitle()->first()->translate(app()->getLocale())->text : "";
    }

    public function getSecondTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(1)->first() ?
            $this->getTitle()->skip(1)->first()->translate(app()->getLocale())->text : "") : "";
    }

    public function getThirdTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(2)->first() ?
            $this->getTitle()->skip(2)->first()->translate(app()->getLocale())->text : "") : "";
    }

    public function getForthTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(3)->first() ?
            $this->getTitle()->skip(3)->first()->translate(app()->getLocale())->text : "") : "";
    }

    public function getFifthTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(4)->first() ?
            $this->getTitle()->skip(4)->first()->translate(app()->getLocale())->text : "") : "";
    }

    public function getSixthTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(5)->first() ?
            $this->getTitle()->skip(5)->first()->translate(app()->getLocale())->text : "") : "";
    }

    public function getSeventhTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(6)->first() ?
            $this->getTitle()->skip(6)->first()->translate(app()->getLocale())->text : "") : "";
    }

    public function getEighthTitle(){
        return $this->getTitle() ? (
        $this->getTitle()->skip(7)->first() ?
            $this->getTitle()->skip(7)->first()->translate(app()->getLocale())->text : "") : "";
    }

}
