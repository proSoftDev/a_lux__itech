<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Vacancy extends Model
{
    use Translatable;
    protected $translatable = ['name', 'experience_required', 'responsibilities', 'text_bottom'];

    public static function getAll(){
        return self::orderBy('sort', 'ASC')->get();
    }
}
