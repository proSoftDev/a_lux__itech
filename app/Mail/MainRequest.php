<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 13.06.2020
 * Time: 22:55
 */

namespace App\Mail;


use App\MainRequest as RequestForMain;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MainRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var RequestForMain
     */
    public $model;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RequestForMain $model)
    {
        $this->model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.main')->subject("Пользователь хочет связаться с вами (Главная)");
    }
}
