<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ProductContentImageList extends Model
{

    public function imageList(){
        return $this->hasOne('App\ProductImageList','id', 'product_image_list_id');
    }

}
